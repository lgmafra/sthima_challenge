from django.db import models
from todo_list.managers import TaskToDoManager

"""
Status used by task_done field
"""
STATUS_TASKS = (
    (False, 'Doing'),
    (True, 'Done')
)


class TaskToDo(models.Model):
    """
    class model used to create a database table
    to store the task registered in app
    """

    task_id = models.AutoField(primary_key=True)
    task_order = models.IntegerField('Order', null=False, blank=False)
    task_title = models.CharField('title', max_length=50, null=True, blank=False)
    task_description = models.CharField('Description', max_length=150, null=False, blank=False)
    task_done = models.BooleanField('Status', null=False, choices=STATUS_TASKS, default=STATUS_TASKS[0])

    objects = TaskToDoManager()

    def __unicode__(self):
        return self.task_description

    class Meta:
        verbose_name = 'To Do List'
        verbose_name_plural = 'To Do List'
