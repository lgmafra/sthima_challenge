from django.test import TestCase
from django.test.client import Client
from django.urls import reverse

from model_mommy import mommy

from todo_list.models import TaskToDo


class HomeViewTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.tasks_done = mommy.make(TaskToDo, task_done=True, _quantity=4)
        self.tasks_doing = mommy.make(TaskToDo, task_done=False, _quantity=6)
        self.data = {
            'task_title': 'Test Title',
            'task_description': '',
            'task_order': 1,
            'task_done': False
        }

    def tearDown(self):
        TaskToDo.objects.all().delete()
        self.data.clear()

    def test_home_status_code(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_form_required_field(self):
        path = reverse('create')
        response = self.client.post(path, self.data)
        self.assertFormError(response, 'form', 'task_description', 'This field is required.')

    def test_form_create_success(self):
        self.data['task_description'] = 'Test Description'

        response = self.client.post(reverse('create'), self.data)
        self.assertEqual(response.status_code, 302)

        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)

    def test_tasks_search(self):
        task_doing = TaskToDo.objects.filter(task_done=False)
        self.assertEqual(len(task_doing), 6)
        task_done = TaskToDo.objects.filter(task_done=True)
        self.assertEqual(len(task_done), 4)
