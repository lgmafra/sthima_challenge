from django.db.models import manager


class TaskToDoManager(manager.Manager):

    """
    function used to change a task status to done or doing
    """
    def change_task_status(self, task_id):
        task = self.get_queryset().get(task_id=task_id)

        return self.get_queryset().filter(task_id=task_id).update(task_done=(not task.task_done))

    """
    function used to delete a task
    """
    def delete_task(self, task_id):
        return self.get_queryset().filter(task_id=task_id).delete()

    """
    function used to change the order between two task
    """
    def change_order(self, task_main, task_to_change):
        order_main_task = self.get_queryset().get(task_id=task_main).task_order
        order_change_task = self.get_queryset().get(task_id=task_to_change).task_order

        return (self.get_queryset().filter(task_id=task_main).update(task_order=order_change_task)
                and self.get_queryset().filter(task_id=task_to_change).update(task_order=order_main_task))
