$(document).ready(function(){
    $('.delete_task').click(function () {
        var task_id = $(this).attr('id');
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_DANGER,
            title: "Attention",
            message: "Are you sure want delete this task?",
            buttons: [{
                label: 'Yes',
                action: function (dialog) {
                    dialog.close();
                    delete_task(task_id);
                }
            },{
                label: 'No',
                action: function(dialog){
                    dialog.close();
                }
            }]
        });
    });

    function delete_task(task_id){
        $.ajax({
            type: 'GET',
            url: "/todolist/task/delete/"+task_id,
            dataType: "text",
            success: function (data) {
                if(data == "ok"){
                    location.href = "/todolist/";
                }
            }
        });
    }
});