from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name='home'),
    path('task/new/', views.create, name='create'),
    path('task/<int:task_id>', views.update, name='update'),
    path('task/delete/<int:task_id>', views.delete, name='delete'),
    path('task/changestatus/<int:task_id>', views.change_status, name='change_status'),
    path('task/changeorder/<int:task_id>', views.change_task_order, name='change_task_order')
]
