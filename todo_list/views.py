from django.db.models import Max
from django.db.models.functions import Coalesce
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.urls import reverse
from todo_list.forms import TaskToDoForm
from todo_list.models import TaskToDo

"""
Views used to implement TODO list actions
"""


def home(request):
    """
    View used to list all tasks registered by user
    """
    tasks = TaskToDo.objects.all().order_by('task_done', 'task_order')

    return render(request, 'home.html', locals())


def create(request):
    """
    View used to create a new task
    """
    task = TaskToDo()
    max_task = TaskToDo.objects.all().aggregate(max_order=Coalesce(Max('task_order'), 0))['max_order']
    task.task_order = max_task + 1

    if request.method == 'POST':
        form = TaskToDoForm(request.POST, instance=task)

        if form.is_valid():
            form.save()
            messages.success(request, 'Task saved successfully')
            return HttpResponseRedirect(reverse('home'))
    else:
        form = TaskToDoForm(instance=task)

    return render(request, 'create.html', locals())


def update(request, task_id):
    """
    View used to update some task
    """
    task = get_object_or_404(TaskToDo, task_id=task_id)

    if request.method == 'POST':
        form = TaskToDoForm(request.POST, instance=task)

        if form.is_valid():
            form.save()
            messages.success(request, 'Task updated successfully')
            return HttpResponseRedirect(reverse('home'))
    else:
        form = TaskToDoForm(instance=task)

    return render(request, 'update.html', locals())


def delete(request, task_id):
    if TaskToDo.objects.delete_task(task_id):
        messages.success(request, 'Task deleted successfully')
        return HttpResponse('ok')
    else:
        messages.error(request, 'Error while delete the task', extra_tags="danger")

    return HttpResponseRedirect(reverse('home'))


def change_status(request, task_id):
    """
    View used to change the status to doing or done
    """
    if TaskToDo.objects.change_task_status(task_id):
        messages.success(request, 'Task status changed successfully')
    else:
        messages.error(request, 'Error while change the task status', extra_tags="danger")

    return HttpResponseRedirect(reverse('home'))


def change_task_order(request, task_id):
    """
    View used to change the order between two tasks
    The user will be informed the tasks id to change the orders
    """
    task = get_object_or_404(TaskToDo, task_id=task_id)

    tasks_to_change = TaskToDo.objects.filter(task_done=False).exclude(task_id=task_id)

    if request.method == 'POST':
        if TaskToDo.objects.change_order(task_id, request.POST['task_to_change']):
            messages.success(request, 'Task order changed successfully')
        else:
            messages.error(request, 'Error while change the task order', extra_tags="danger")

        return HttpResponseRedirect(reverse('home'))

    return render(request, 'change_order.html', locals())
