from django import forms

from todo_list.models import TaskToDo


class TaskToDoForm(forms.ModelForm):
    """
    class used to generate the html form
    used to interact in the app views
    """

    class Meta:
        model = TaskToDo
        exclude = []

    def __init__(self, *args, **kwargs):
        super(TaskToDoForm, self).__init__(*args, **kwargs)
        self.fields['task_order'].widget.attrs['readonly'] = True

        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
