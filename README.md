# TODO List Challenge

Web application for a technical challenge for job opportunity as a developer

The application is running on Heroku in the url: sthima-challenge.herokuapp.com/todolist/

# Requirements
	* Python 3.6
	* Django 2.0.7
	* model_mommy
	
# Installation / Instructions for use
-----------------------------
	* Installation

clone the project

```shell
$ git clone https://lgmafra@bitbucket.org/lgmafra/olist_test.git
```

Install the pip requirements

```shell
$ pip3 install -r requirements.txt
```

Run migrations

```shell
$ python3 manage.py migrate
```

Run application
```shell
$ python3 manage.py runserver
```

-----------------------------
	* Instructions for use

* Use "New Task" button to create a task todo;
* Use "Edit Task" button to edit a task information and change the status for done or doing;
* Use "Delete Task" button to remove a task;
* Use "Status" button to change the status tasks;
* Use "Change Task Order" button to change the clicked task order for another one task order.


# Brief description
* MacBook Air / Mac OS High Sierra 10.13.6;
* PyCharm Professional.
